package edu.buffalo.cse116;

import java.util.ArrayList;
import java.util.Random;

public class Cards {
	/*
	 * Weapons:
	 * - Candlestick
	 * - Wrench
	 * - Rope
	 * - Revolver
	 * - Knife
	 * - Lead Pipe
	 * 
	 * Rooms:
	 * - Conservatory
	 * - Lounge
	 * - Library
	 * - Kitchen
	 * - Ballroom
	 * - Hall
	 * - Billiard Room
	 * - Study
	 * - Dining Room
	 * 
	 * Characters:
	 * - Professor Plum
	 * - Mrs. White
	 * - Mr. Green
	 * - Mrs. Peacock
	 * - Miss Scarlett
	 * - Colonel Mustard
	 */
	
	/*
	 * We construct the deck from three sub-decks consisting of character cards, room cards, and weapon cards.
	 * * * *
	 */
	ArrayList<String> character_cards = new ArrayList<String>();
	ArrayList<String> room_cards = new ArrayList<String>();
	ArrayList<String> weapon_cards = new ArrayList<String>();
	
	//populating the deck
	public Cards(){
		character_cards.add("Professor Plum");
		character_cards.add("Mrs. White");
		character_cards.add("Mr. Green");
		character_cards.add("Mrs. Peacock");
		character_cards.add("Miss Scarlett");
		character_cards.add("Colonel Mustard");
		
		room_cards.add("Conservatory");
		room_cards.add("Lounge");
		room_cards.add("Library");
		room_cards.add("Kitchen");
		room_cards.add("Ballroom");
		room_cards.add("Hall");
		room_cards.add("Billiard Room");
		room_cards.add("Study");
		room_cards.add("Dining Room");
		
		weapon_cards.add("Candlestick");
		weapon_cards.add("Wrench");
		weapon_cards.add("Rope");
		weapon_cards.add("Revolver");
		weapon_cards.add("Knife");
		weapon_cards.add("Lead Pipe");
	}
	
	public ArrayList<String> get_envelope(){
		// Returns the 'envelope' containing one random card
		// from each category.
		
		ArrayList<String> env = new ArrayList<String>();
		env.add(draw_card('c'));
		env.add(draw_card('r'));
		env.add(draw_card('w'));
		
		return env;
	}
	
	public String draw_card(char c){
		//draws a card from the specified category
		/*
		 * c = Character
		 * r = Room
		 * w = Weapon
		 * 
		 * This takes the sub-deck specified by 'c', randomly selects a card from it,
		 * removes the card, and returns it.
		 */
		
		ArrayList<String> cat = new ArrayList<String>();
		
		switch(c){
		
		case 'c': 
			cat = this.character_cards;
			break;
		case 'r':
			cat = this.room_cards;
			break;
		case 'w':
			cat = this.weapon_cards;
			break;
		}
		
		Random rand = new Random();
		int index = rand.nextInt(cat.size());
		String card = cat.get(index);
		cat.remove(index);
		return card;
	}
}
