package edu.buffalo.cse116;

import org.junit.Test;

import edu.buffalo.cse116.Player.MismatchedTupleSizes;

public class BoardTest {
	//@Test
	public void RollDieTest(){
		Board b = new Board();
		System.out.println("Rolled Value: " + b.rollDie() + '\n');
	}
	
	//@Test
	public void BoardArrayTest(){
		Board b = new Board();
		b.printBoard();
	}
	
	//@Test
	public void getEnvelopeTest(){
		Board b = new Board();
		System.out.println(b.envelope);
	}
	
	//@Test
	public void drawCardTest(){
		Cards deck = new Cards();
		for (int i = 0; i < 6; i++){
			System.out.println(deck.draw_card('w'));
		}
	}
	
	//@Test
	public void movementTest(){
		Board b = new Board();
		Player player = b.players.get(0);
		System.out.println(player.p[0] + " " + player.p[1]);
		Boolean val = player.moveTo(23, 8, 6);
		System.out.println(val);
	}
	
	@Test
	public void _vaddTest() throws MismatchedTupleSizes{
		Player p = new Player(0);
		int[] v = {1,1};
		System.out.println(p.p[0] + ", " + p.p[1]);
		p.move(v);
		System.out.println(p.p[0] + ", " + p.p[1]);
	}
	
}
