package edu.buffalo.cse116;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Board {
	/* Board Class
	The Board class is designed to be an all encompassing set of parameters for the game.
	Basically, by this design, everything that doesn't belong in some other class will go here,
	and the other classes will refer to the board for these parameters.
	
	In terms of the 'physical' board, I'm using
	https:s-media-cache-ak0.pinimg.com/originals/8d/4a/95/8d4a95cfddea78bcee58a7eb243ceb93.jpg
	as reference (although I think all the boards are designed the same).
	*/
	
	/* board_grid
	In order to allow things like movement or postion-based actions (such as entering a room),
	we need to know what each point of the board is (such as being an empty space or being a
	part of a room).  Below is a 24x25 grid of integers representing the board.
	At this point, this can be used for reference, but it will have to be used to implement
	mechanics into the game.  Although we are not concerned with any interface yet,
	this grid can be used to generate graphics, so it will be useful later.
	It can be printed to the console using the classes "printBoard" method.
	
	KEY:
	1: Movable Space
	2: Door
	3: Room
	4: Passage
	0: Non-Valid Space
	*/
	
	// constructor
	ArrayList<String> envelope;
	List<Player> players = new ArrayList<Player>();
	Cards deck = new Cards();
	
	public Board(){
		makePlayers();
		// This the 'goal' envelope containing a character, room, and weapon
		envelope = deck.get_envelope();
	}
	
	int[][] board_grid = {
			{3,3,3,3,3,3,0,		1,0,	3,3,3,3,3,3,	0,1,	0,3,3,3,3,3,3},
			{3,3,3,3,3,3,3,		1,1,	3,3,3,3,3,3,	1,1,	3,3,3,3,3,3,3},
			{3,3,3,3,3,3,3,		1,1,	3,3,3,3,3,3,	1,1,	3,3,3,3,3,3,3},
			{4,3,3,3,3,3,3,		1,1,	3,3,3,3,3,3,	1,1,	3,3,3,3,3,3,3},
			
			{0,1,1,1,1,1,1,		1,1,	2,3,3,3,3,3,	1,1,	3,3,3,3,3,3,3},
			{1,1,1,1,1,1,1,		1,1,	3,3,3,3,3,3,	1,1,	2,3,3,3,3,3,4},
			
			{0,3,3,3,3,3,1,		1,1,	3,3,2,2,3,3,	1,1,	1,1,1,1,1,1,0},
			{3,3,3,3,3,3,3,		1,1,	1,1,1,1,1,1,	1,1,	1,1,1,1,1,1,1},
			{3,3,3,3,3,3,2,		1,1,	0,0,0,0,0,1,	1,1,	1,1,1,1,1,1,0},
			{3,3,3,3,3,3,3,		1,1,	0,0,0,0,0,1,	1,3,	2,3,3,3,3,3,3},
			{0,3,3,2,3,3,1,		1,1,	0,0,0,0,0,1,	1,3,	3,3,3,3,3,3,3},
			
			{0,1,1,1,1,1,1,		1,1,	0,0,0,0,0,1,	1,3,	3,3,3,3,3,3,3},
			
			{3,2,3,3,3,3,1,		1,1,	0,0,0,0,0,1,	1,2,	3,3,3,3,3,3,3},
			{3,3,3,3,3,3,1,		1,1,	0,0,0,0,0,1,	1,3,	3,3,3,3,3,3,3},
			{3,3,3,3,3,3,1,		1,1,	0,0,0,0,0,1,	1,3,	3,3,3,3,3,3,3},
			{3,3,3,3,3,2,1,		1,1,	1,1,1,1,1,1,	1,1,	1,1,3,3,3,3,3},
			{3,3,3,3,3,3,1,		1,1,	1,1,1,1,1,1,	1,1,	1,1,1,1,1,1,0},
			
			{0,1,1,1,1,1,1,		1,3,	2,3,3,3,3,2,	3,1,	1,1,1,1,1,1,1},
			{1,1,1,1,1,1,1,		1,3,	3,3,3,3,3,3,	3,1,	1,3,2,3,3,3,0},
			
			{0,4,3,3,2,1,1,		1,2,	3,3,3,3,3,3,	2,1,	1,3,3,3,3,3,3},
			{3,3,3,3,3,3,1,		1,3,	3,3,3,3,3,3,	3,1,	1,3,3,3,3,3,3},
			{3,3,3,3,3,3,1,		1,3,	3,3,3,3,3,3,	3,1,	1,3,3,3,3,3,3},
			{3,3,3,3,3,3,1,		1,3,	3,3,3,3,3,3,	3,1,	1,3,3,3,3,3,3},
			{3,3,3,3,3,3,0,		1,1,	1,3,3,3,3,1,	1,1,	0,4,3,3,3,3,3},
			{0,0,0,0,0,0,0,		0,0,	1,0,0,0,0,1,	0,0,	0,0,0,0,0,0,0}
	};
	
	
	public void printBoard(){
		int point;
		for (int y = 0; y < 25; ++y){
			for (int x = 0; x < 24; ++x){
				point = this.board_grid[y][x];
				for (Player p:this.players){
					if (p.x == x && p.y == y){
						System.out.print(p.col+"|");
						point = 5;
					}
				}
				if (point == 4){
					System.out.print("?|");
				}
				if (point == 3){
					System.out.print("#|");
				}
				if (point == 2){
					System.out.print("/|");
				}
				if (point == 1){
					System.out.print("_|");
				}
				if (point == 0){
					System.out.print("  ");
				}
				
			}
			System.out.print("\n");
		}
	}
	
	public void makePlayers(){
		for (int i = 0; i < 6; ++i){
			players.add(new Player(i));
		}
	}
	
	public int rollDie(){
		//Returns a random integer between (and including) 1 and 6
		Random rand = new Random();
		return rand.nextInt(6)+1;
	}
}
