package edu.buffalo.cse116;

public class Player {
	int id;
	String name;
	String color;
	char col; //alternative to color

	int[] p = {0,0};
	
	public Player(int id){
		switch(id){
		
		case 0: this.name = "Colonel Mustard";
		this.color = "yellow";
		this.col = 'y';
		this.p[0] = 23;
		this.p[1] = 7;
		break;
		
		case 1: this.name = "Miss Scarlet";
		this.color = "red";
		this.col = 'r';
		this.p[0] = 16;
		this.p[1] = 0;
		break;
		
		case 2: this.name = "Professor Plum";
		this.color = "purple";
		this.col = 'p';
		this.p[0] = 0;
		this.p[1] = 5;
		break;
		
		case 3: this.name = "Mr. Green";
		this.color = "green";
		this.col = 'g';
		this.p[0] = 9;
		this.p[1] = 24;
		break;
		
		case 4: this.name = "Mrs. White";
		this.color = "white";
		this.col = 'w';
		this.p[0] = 14;
		this.p[1] = 24;
		break;
		
		case 5: this.name = "Mrs. Peacock";
		this.color = "blue";
		this.col = 'b';
		this.p[0] = 0;
		this.p[1] = 18;
		break;
		}
	}
	
	public class MismatchedTupleSizes extends Exception {

		  public MismatchedTupleSizes(int a, int b){
		     super("Mismatched Tuple Sizes: " + a + " and " + b);
		  }

		}
	
	public int[] vadd(int[] p, int[] v) throws MismatchedTupleSizes{
		/*
		 * I don't know what Java has for vector operations,
		 * but this will take care of tuple addition
		 */
		
		if (p.length != v.length){
			throw new MismatchedTupleSizes(p.length, v.length);
		}
		
		for (int i = 0; i < p.length; ++i){
			p[i] = p[i] + v[i];
		}
		
		return p;
	}
	
	public void move(int[] v) throws MismatchedTupleSizes{
		this.p = vadd(this.p, v);
	}
	
	public int[] _nextMove(int[] v){
		//Increments the vector to the next move
		int[] u = {0,1}, d = {0,-1}, l = {-1,0}, r = {1,0};
		if (v == u){
			return d;
		}
		if (v == d){
			return l;
		}
		if (v == l){
			return r;
		}
		if (v == r){
			return u;
		}
		//Dont let it get this far
		int[] fallback = {0,0};
		return fallback;
	}
	
	public int[][] nextPerm(int[][] perm){
		/*
		 * Find all permutations of an array of moves
		 */
		int[] u = {0,1}, d = {0,-1}, l = {-1,0}, r = {1,0};
		for (int i = 0; i < perm.length; ++i){
			//TODO
		}
		return perm;
	}
	
	public boolean moveTo(int x, int y, int moves){
		/* Returns true if character can move to the spot, False if else
		 * I'm trying to figure out a descent way of doing this, but without
		 * knowing how the UI is going to turn out, it's up in the air.
		 * 
		 * For now, the idea is that, at most, the player can roll a 6.
		 * The player can move the character up, down, left, right.
		 * This is 4^6 = 4096 possible moves (counting bogus ones, too).
		 * That's not too bad to compute on the fly, but it definitely
		 * seems poorly implemented.
		 * 
		 * int x: x-coordinate of desired location
		 * int y: y-coordinate of desired location
		 * int moves: number of moves available (via dice roll)
		 */
		
		if (Math.abs(this.p[0]-x)+Math.abs(this.p[1]-y) > moves){
			//No way to move their based on number of moves available,
			return false;
		}
		
		int[] p0 = this.p;
		int[] u = {0,1}, d = {0,-1}, l = {-1,0}, r = {1,0};
		int[][] mvmnts = {u,d,l,r};
		int[][] perms;
		
		return true;
	}
}
